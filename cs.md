# Markdown Cheat Sheet

Det här dokumentet är skrivet i ett format som kallas *Markdown*. Markdown är ett enkelt sätt att formattera textdokument.

## Rubriker

För att göra en rubrik skriver man en hash `#` i början av raden, som i början av rubriken i detta avsnitt.

Ju fler `#` i början av raden, desto mindre rubrik.

Skriv en `#` i början av raden där det står *Rubrik 1*, `##` på raden
*Rubrik 3* och `###` på raden *Rubrik 3*.

Rubrik 1  

Rubrik 2  

Rubrik 3  

## Betoning

För att få viss ord att sticka ut kan man betona det genom att skriva en
asterisk `*` framför och efter ordet.

Betona ordet emphasis i denna mening genom att omge det med `*`.

Betona orden strong emphasis i denna mening genom att omge dem båda med `**`.

## Listor

Det finns två typer av listor:

* Punktlistor
* Numrerade listor

Punktlistor börjar med en asterisk på varje rad. Man kan göra en fruktsallad av
ingredienserna nedan. Gör om dem till en punktlista.

Banan  
Vindruvor  
Kiwi  
Passionsfrukt  
Mango  

Numrerade listor börjar med nummer och punkt. Listan nedan är inte komplett.
Lägg till punkterna 3 - 5 så att det blir rätt.

1. Skala bananen och skär den i skivor.
2. Skölj vindruvorna och dela dem.
Skala kiwi och skär i tärningar.  
Gröp ur passionsfrukten.  
Skala mangon och skär i stavar.  
6. Blanda alla frukter i en skål.

## Kod

Genom att börja en rad med fyra blanksteg eller en tab gör man att texten blir ett kodblock.

Skriv 4 blanksteg i början av de här två raderna
för att göra ett kodblock.





